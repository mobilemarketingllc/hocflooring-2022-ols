<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

add_action( 'wp_enqueue_scripts', function(){
wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/resources/slick/script.js","","",1);
wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.css");
wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
});

// Classes
require_once 'classes/fl-class-child-theme.php';



// add_action( 'after_setup_theme',     'FLChildTheme::setup' );
// add_action( 'init',                  'FLChildTheme::init_woocommerce' );
 add_action( 'wp_enqueue_scripts',    'FLChildTheme::enqueue_scripts', 999 );
 add_action( 'widgets_init',          'FLChildTheme::widgets_init' );
add_action( 'wp_footer',             'FLChildTheme::go_to_top' );

// // Theme Filters
// add_filter( 'body_class',            'FLChildTheme::body_class' );
 //add_filter( 'excerpt_more',          'FLChildTheme::excerpt_more' );

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 ); 

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// add_filter('show_admin_bar', '__return_false');


//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
  if ( isset( $atts['facet'] ) ) {       
      $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
  }
  return $output;
}, 10, 2 );


//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail_new' );

function wpse_100012_override_yoast_breadcrumb_trail_new( $links ) {
    global $post;
    
    if ( is_singular( 'hardwood_catalog' )  ) {
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring-options/',
                'text' => 'Flooring Options',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring-options/hardwood-flooring/',
                'text' => 'Hardwood Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring-options/hardwood-flooring/browse-hardwood/',
                'text' => 'Browse Hardwood',
            );
        array_splice( $links, 1, -1, $breadcrumb );

    }elseif (is_singular( 'carpeting' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-options/',
            'text' => 'Flooring Options',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-options/carpets/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-options/carpets/browse-carpet/',
            'text' => 'Browse Carpet',
        );
        array_splice( $links, 1, -1, $breadcrumb );

    }elseif (is_singular( 'luxury_vinyl_tile' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-options/',
            'text' => 'Flooring Options',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-options/luxury-vinyl/',
            'text' => 'Luxury Vinyl Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-options/luxury-vinyl/browse-luxury-vinyl-tile/',
            'text' => 'Browse Luxury Vinyl Tile',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'laminate_catalog' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-options/',
            'text' => 'Flooring Options',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-options/laminate-flooring/',
            'text' => 'Laminate Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-options/laminate-flooring/browse-laminate/',
            'text' => 'Browse Laminate',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'tile_catalog' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-options/',
            'text' => 'Flooring Options',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-options/tile-flooring/',
            'text' => 'Tile Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-options/tile-flooring/browse-tile/',
            'text' => 'Browse Tile',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    return $links;
}

// Vendor page product list

function brand_products_function($atts){

    global $wpdb;

    //write_log( $atts);
    $args = array(
        "post_type" =>  array('luxury_vinyl_tile', 'laminate_catalog', 'hardwood_catalog', 'tile_catalog', 'carpeting'),
        "post_status" => "publish",       
        "order" => "rand",
        "meta_key" => 'collection',
        "orderby" => 'meta_value', 
        "posts_per_page" => 4,
      'meta_query' => array(
                array(
                  'key' => 'brand',
                  'value' => $atts[0],
                  'compare' => '='
                  )
         )
      );

    //  write_log( $args);
    add_filter('posts_groupby', 'search_distinct');
    //remove_filter('posts_groupby', 'query_group_by_filter');
      $the_query = new WP_Query( $args );
    
    $content = '<div class="product-plp-grid product-grid swatch related_product_wrapper brandpro_wrapper">				
                <div class="row product-row">';				
                                
              
                while ($the_query->have_posts()) {
                $the_query->the_post();
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
                $style = "padding: 5px;";
       
    $content .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="fl-post-grid-post">                
                    <div class="fl-post-grid-image">
                    <a itemprop="url" href="'.get_the_permalink().'" title="'.get_the_title().'">
                    <img class="list-pro-image" src="'.$image.'" alt="'.get_the_title().'"/>';
                          
                            
  
                         $content .='</a></div>';

                    $table_posts = $wpdb->prefix.'posts';
                    $table_meta = $wpdb->prefix.'postmeta';
                    $familycolor = get_field('collection');  
                    $key = 'collection';  

                    $coll_sql = "select distinct($table_meta.post_id)  
                                FROM $table_meta
                                inner join $table_posts on $table_posts.ID = $table_meta.post_id 
                                WHERE meta_key = '$key' AND meta_value = '$familycolor'";

                           //     write_log($coll_sql);

                    $data_collection = $wpdb->get_results($coll_sql);

                    
    
    $content .=     '<div class="fl-post-grid-text product-grid btn-grey">
                            <h4><a href="'.get_the_permalink().'" title="'.get_the_title().'" class="collection_text">'.get_field('collection').'</a> 
                            <a href="'.get_the_permalink().'" title="'.get_the_title().'"> <span class="color_text">'.get_field('color').'</span></a>
                                <span class="brand_text">'.get_field('brand').' </span> </h4>
                        </div>    
                        
                        <div class="product-variations1"> <h5>'.count($data_collection).' COLORS AVAILABLE</h5></div>

                </div>
            </div>';
           
    } 
       
    wp_reset_postdata(); 
    
    remove_filter('posts_groupby', 'search_distinct');                  
    $content .= '</div>
            </div>';
            
            return $content;
    
    } 
    
    add_shortcode( 'brand_listing', 'brand_products_function' );
   

    function search_distinct() {

        global $wpdb;    
        return $wpdb->postmeta . '.meta_value ';  
    }