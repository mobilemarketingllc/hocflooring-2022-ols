<?php
//$show_thumbs = FLTheme::get_setting('fl-posts-show-thumbs');
?>
<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/Product">
    <div class="fl-post-content clearfix " itemprop="text">
        <?php get_template_part('includes/product-color-slider');   ?>
        <div class="productInfo row productDesktop">
			<div class="container">
            <div class="colorTxt">Color</div>
            <h2 class="colorName"><?php echo get_field('color');?></h2>
            <div class="col-md-4 col-sm-4">
                <div class="brandLogo">
                    <?php
                
                if (get_field('brand') == 'Karastan'){ ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_karastan.png" class="product-logo" class="img-responive"/>

                <?php } elseif (get_field('brand') == 'SmartSolutions') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>

                <?php } elseif (get_field('brand') == 'STAINMASTER') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/stainmaster_logo.png" class="product-logo" class="img-responive"/>

                <?php } elseif (get_field('brand') == 'Armstrong') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/armstronglogo.png" class="product-logo" class="img-responive"/>

                <?php } elseif (get_field('brand') == 'Quick-Step') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/quickstep-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Daltile') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/daltile-logo.gif" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Floorscapes') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/floorscapes.jpg" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'OpenLine') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'ColorCenter') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-color-center.jpg" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Mohawk') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'NFA') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
                        <?php } elseif (get_field('brand') == 'Hallmark Floors') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Shaw') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/shaw-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Bruce') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bruce-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Mannington') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mannington-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'IVC') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ivcus-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Congoleum') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/congoleum-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Aladdin Commercial') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/aladdin-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'California Classics') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/californiaClassics.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Hallmark') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hallmark.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Reward') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/reward.jpg" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Monarch') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/monarch.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Republic') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/republic.jpg" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Emser') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_emserTile.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Marazzi') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_marazzi.png" class="product-logo" class="img-responive"/>
        
                <?php } else { echo get_field('brand'); }?>


                </div>
                <div class="requestQuote">
                    <a href="/request-quote/">REQUEST A QUOTE</a>        
                </div>

                <div class="scheduleEstimate">
                    <a href="/schedule-an-estimate/">SCHEDULE AN ESTIMATE</a>        
                </div>

                <div class="scheduleEstimate">
                    <a href="/500-off-flooring/">SAVE $500</a>        
                </div>

            </div>
            <div class="col-md-4 col-sm-4">
                <div class="productImg">
                    <?php  if(get_field('swatch_image_link')){ $image_s = swatch_image_product(get_the_ID(),1600,1600);?>            
                    <img id="swathcimg" src="<?php echo $image_s;?>">
                    <?php } else {?>
                    <img id="swathcimg" src="https://placehold.it/400x400?text=NO+IMAGE">
                    <?php } if(get_field('room_scene_image_link')){ $image_r = high_gallery_images_in_loop(get_field('room_scene_image_link'));?>
                    <img id="roomimg" src="<?php echo $image_r;?>"> 
                    <?php } else {?>
                    <img id="roomimg" src="https://placehold.it/400x400?text=NO+IMAGE">
                    <?php } ?>
                </div>       
                    <div class="srlinks">
                    <span class="swatch Imgactive"><i class="fa fa-angle-up"></i>SWATCH</span><span class="room"><i class="fa fa-angle-up"></i> ROOM</span> 
                    </div>    
            </div>
            <div class="col-md-4 col-sm-4">
            <?php if(get_field('style')){?>
                 <div class="attributeInfo">
                   <span class="leftattribute">STYLE</span>
                   <span class="rightattribute"><?php echo get_field('style');?></span>
                 </div> 
            <?php } if(get_field('color')) {?>    
                 <div class="attributeInfo">
                   <span class="leftattribute">COLOR</span>
                   <span class="rightattribute"><?php echo get_field('color');?></span>
                 </div> 
            <?php } if(get_field('fiber_type')){?>    
                 <div class="attributeInfo">
                   <span class="leftattribute">FIBER TYPE</span>
                   <span class="rightattribute"><?php echo get_field('fiber_type');?></span>
                 </div>
            <?php }?>  
            <div class="showmore">
            <?php if(get_field('fiber_type')){?>    
                 <div class="attributeInfo">
                   <span class="leftattribute">Collection</span>
                   <span class="rightattribute"><?php echo get_field('collection');?></span>
                 </div>
            <?php } if(get_field('fiber_type')){?>
                <div class="attributeInfo">
                   <span class="leftattribute">Brand</span>
                   <span class="rightattribute"><?php echo get_field('brand');?></span>
                 </div>
            <?php } ?>
            </div>   

            <div class="viewmore">+ view more</div>          
            </div>

		</div>
        </div>

        <div class="productInfo row productMobile">
            <div class="colorTxt">Color</div>
            <h2 class="colorName"><?php echo get_field('color');?></h2>
            <div class="productImage">
                <div class="productImg">
                    <?php  if(get_field('swatch_image_link')){?>            
                    <img id="swathcimgM" src="https://mobilem.liquifire.com/mobilem?source=url[<?php echo get_field('swatch_image_link');?>]&sink">
                    <?php } else {?>
                    <img id="swathcimgM" src="https://placehold.it/400x400?text=NO+IMAGE">
                    <?php } if(get_field('room_scene_image_link')){?>
                    <img id="roomimgM" src="https://mobilem.liquifire.com/mobilem?source=url[<?php echo get_field('room_scene_image_link');?>]&sink"> 
                    <?php } else {?>
                    <img id="roomimgM" src="https://placehold.it/400x400?text=NO+IMAGE">
                    <?php } ?>
                </div>       
                    <div class="srlinks">
                    <span class="swatchM ImgactiveM"><i class="fa fa-angle-up"></i>SWATCH</span><span class="roomM"><i class="fa fa-angle-up"></i> ROOM</span> 
                    </div>    
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="brandLogo">
                    <?php
                
                if (get_field('brand') == 'Karastan'){ ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_karastan.png" class="product-logo" class="img-responive"/>

                <?php } elseif (get_field('brand') == 'SmartSolutions') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>

                <?php } elseif (get_field('brand') == 'STAINMASTER') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/stainmaster_logo.png" class="product-logo" class="img-responive"/>

                <?php } elseif (get_field('brand') == 'Armstrong') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/armstronglogo.png" class="product-logo" class="img-responive"/>

                <?php } elseif (get_field('brand') == 'Quick-Step') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/quickstep-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Daltile') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/daltile-logo.gif" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Floorscapes') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/floorscapes.jpg" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'OpenLine') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'ColorCenter') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-color-center.jpg" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Mohawk') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'NFA') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
                        <?php } elseif (get_field('brand') == 'Hallmark Floors') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Shaw') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/shaw-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Bruce') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bruce-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Mannington') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mannington-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'IVC') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ivcus-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Congoleum') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/congoleum-logo.png" class="product-logo" class="img-responive"/>
                <?php } elseif (get_field('brand') == 'Aladdin Commercial') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/aladdin-logo.png" class="product-logo" class="img-responive"/>
        
                <?php } else { echo get_field('brand'); }?>


                </div>
                <div class="requestQuote">
                    <a href="/request-quote/">REQUEST A QUOTE</a>        
                </div>

                <div class="scheduleEstimate">
                    <a href="/schedule-an-estimate/">SCHEDULE AN ESTIMATE</a>        
                </div>

                <div class="scheduleEstimate">
                    <a href="/500-off-flooring/">SAVE $500</a>    
                        
                </div>
                
            </div>
            
            <div class="col-md-4 col-sm-4">
            <div style="height:50px; clear:both;">&nbsp;</div>
            <?php if(get_field('style')){?>
                 <div class="attributeInfo">
                   <span class="leftattribute">STYLE</span>
                   <span class="rightattribute"><?php echo get_field('style');?></span>
                 </div> 
            <?php } if(get_field('color')) {?>    
                 <div class="attributeInfo">
                   <span class="leftattribute">COLOR</span>
                   <span class="rightattribute"><?php echo get_field('color');?></span>
                 </div> 
            <?php } if(get_field('fiber_type')){?>    
                 <div class="attributeInfo">
                   <span class="leftattribute">FIBER TYPE</span>
                   <span class="rightattribute"><?php echo get_field('fiber_type');?></span>
                 </div>
            <?php }?>  
            <div class="showmore">
            <?php if(get_field('fiber_type')){?>    
                 <div class="attributeInfo">
                   <span class="leftattribute">Collection</span>
                   <span class="rightattribute"><?php echo get_field('collection');?></span>
                 </div>
            <?php } if(get_field('fiber_type')){?>
                <div class="attributeInfo">
                   <span class="leftattribute">Brand</span>
                   <span class="rightattribute"><?php echo get_field('brand');?></span>
                 </div>
            <?php } ?>
            </div>   

            <div class="viewmore">+ view more</div>          
            </div>

            
        </div>
    <?php //get_template_part('includes/product-attributes'); ?>
    <?php //get_template_part('includes/product-extra-info');   ?>
    </div>
</article>
<script>
jQuery( ".swatch" ).click(function() {

    jQuery( "#roomimg" ).hide();
    jQuery( ".room i" ).hide();
    jQuery( ".swatch i" ).show();
    jQuery( "#swathcimg" ).show();
    jQuery(this).addClass( "Imgactive" );
    jQuery( ".room" ).removeClass("Imgactive");
    
});
jQuery( ".room" ).click(function() {

    jQuery( "#swathcimg" ).hide();
    jQuery( ".swatch i" ).hide();
    jQuery( ".room i" ).show();
    jQuery( "#roomimg" ).show();
    jQuery(this).addClass( "Imgactive" );
    jQuery( ".swatch" ).removeClass("Imgactive");
});

/* Mobile Js */
jQuery( ".swatchM" ).click(function() {

jQuery( "#roomimgM" ).hide();
jQuery( ".roomM i" ).hide();
jQuery( ".swatchM i" ).show();
jQuery( "#swathcimgM" ).show();
jQuery(this).addClass( "ImgactiveM" );
jQuery( ".roomM" ).removeClass("ImgactiveM");

});
jQuery( ".roomM" ).click(function() {

jQuery( "#swathcimgM" ).hide();
jQuery( ".swatchM i" ).hide();
jQuery( ".roomM i" ).show();
jQuery( "#roomimgM" ).show();
jQuery(this).addClass( "ImgactiveM" );
jQuery( ".swatchM" ).removeClass("ImgactiveM");
});

jQuery(".viewmore").toggle(function(){
    jQuery(this).text("- view less").siblings(".showmore").show();    
}, function(){
    jQuery(this).text("+ view more").siblings(".showmore").hide();    
});
</script>
